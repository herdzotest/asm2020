<?php include('header.php'); ?>

<section class="flower_section  about_flower_common_section layout_padding">
  <div class="container  ">
    <div class="row">
      <div class="col-md-12">
        <div class="detail-box">
          <div class="heading_container">
            <h2>
              Amuseum Student ART Prize 2020
            </h2>
          </div>
          <div class="img-box" style="text-align: center;">
            <img src="images/asm/student artprize image.png" alt="student artprize image.png" />
          </div>
          <p>
            The world has been going through a pandemic and ‘lockdown’ and ‘quarantine’ have become household words and concepts. As fast adapters of emerging tendencies and trends, children too have adjusted themselves with the new situation while engaging with the world through various mediums of which internet enabled digital platforms play a major role. Their offline activities have resulted into art, journal entries, poems, short stories, photography and even short films. Amuseum Student Art Prize 2020 is an attempt to see what the children of the world have been doing during the last seven months mostly ‘locked up inside homes’. All School Students could participate in this mega art festival by sending in the digital/photographic images of their creative works such as paintings, drawings, collages and sculptures for the Amuseum Student Art Prize 2020.
          </p>
          <h5>
            Categories:
          </h5>
          <p>
            1) Students upto 8 th Forum/ STD (aged up to 13 years) belong to the Junior category.<br>
            2) Students from 9 to 12 Forum/ STD ( 14 years Age or above belong to the Senior category.
          </p>
          <h5>Theme: ‘Locked-in-Home’</h5>
          <h5>Nature of Entries:</h5>
          <p>
            Each participant should send in three images of their works (it could be three different paintings or drawings or collages or sculpture or one image from each category).
          </p>
          <h5>Last Date of Application: 20th December 2020</h5>
          <br />
          <h5>Nature of the Prize:</h5>
          <p>
            1) ‘Black Prize’ and carries a purse of Indian Rupees 20,000/- for Senior Category and Rupees 10000/- for Junior Category and a certificate, portfolio of eminent works of art, and an Amuseum Souvenir. <br>
            2) ‘Yellow Prize’ and carries a purse of Indian Rupees 10,000/- for Senior Category and Rupees 5000/- for Junior Category and a certificate, portfolio of eminent works of art and an Amuseum Souvenir.<br>
            3) ‘Magenta Prize’ and carries a purse of Indian Rupees 5,000/- for Senior Category and Rupees 3000/- for Junior Category and a certificate, portfolio of eminent works of art and an Amuseum Souvenir.<br>
            4) ‘Cyan Prize’ and carries a purse of Indian Rupees 2,000/- for Senior Category and Rupees 1000/- for Junior Category and a certificate, portfolio of eminent works of art and an Amuseum Souvenir. (Cyan Prize is given to 10 selected participants in each category )
          </p>
          <h5>How to Register for Amuseum Student Art Prize 2020?</h5>
          <p>
            You may go through the registration link in the website (www.amuseum.org.in) and follow the instructions. Alternatively Printed Registration form can be filled and scanned to send through Email to amuseumartscience@gmail.com or posted directly to our Office.
            Amuseum ArtScience
            TC 979/26,
            Plamoodu- PMG Highway,
            Pattom Post,
            Thiruvananthapuram,
            695004,
            Kerala. INDIA.
            Phone
            +91 8589061461
          </p>
          <p>
            Registration Fee is Indian Rupees 100/- should be payed with our Bank. Banking Details are as follows <br><br>
            <b>Name: Amuseum Artscience,<br>
              Number:13740200004303 <br>
              Bank: Federal Bank, <br>
              Branch: Pattom, Thiruvananthapuram<br>
              IFSC: FDRL0001374</b>
          </p>
          <h5>Jury Members:</h5>
          <p>
            A two tier jury of eminent artists, scholars and educators will judge the works. Their names will be declared soon. The decision of the Jury will be final on the Awards.
          </p>
          <br />
          <h5>Date of Announcement of the Results: 1st January 2021.</h5><br>
          <h5>
            Additional Takeaways for the Participants:
          </h5>
          <p>There will be two online master classes with the duration of two
            hours each on water colour painting and Portrait painting for
            selected 500 participants. This will be held after the
            announcement of the Prizes in two consecutive weekends. 100 works
            selected by the jury will be on display as a month-long exhibition
            in the Amuseum Online Gallery and included in a limited edition
            Catalogue.</p>
          <h5>For further Details:</h5>
          <p>

            mail to : amuseumartscience@gmail.com <br>
            Whats app: +91 8589061461, +91 9995036666 <br>
            Phone: +91 9995036666
          </p>
        </div>
      </div>
    </div>
    <a href="register.php"><button class="btn btn-success">Register Now</button></a><a target="_blank" href="images/asm/student art prize brochure web.pdf"><button style="margin: 0 0 0 10px;" class="btn btn-primary"> Download Brochure</button></a>
  </div>
</section>
<?php include('footer.php'); ?>