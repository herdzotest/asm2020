<?php include('header.php');?>

    <!-- why section -->

    <section class="why_section layout_padding">
      <div class="container">
        <div class="heading_container">
          <h2>
            Contact Us
          </h2>
          <p>
            For anything you can reach out to us, feel free to ask your doubts,
            will help you in everything for sure.
          </p>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="box pr-0 pr-lg-5">
              <div class="detail-box">
                <p>
                  Amuseum Artscience<br>
                  TC 979/26<br>
                  Plamoodu -PMG Highway<br>
                  Pattom Post, Thiruvananthapuram.<br>
                  Kerala. India. 695004<br>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="box pr-0 pr-lg-5">
              <div class="detail-box">
                <p>Support<br>
                Amuseum Artscience is a Registered Non profiteering Trust, registered in Kerala, India. Registration Number IV/16/2020. Our activities would not be possible without financial support from like minded people. This means enhanced research, displays, archiving and publications from our side. Your support is valuable for us.
                </p>
                <p>
                Do donate @<br>
                Name: Amuseum Artscience<br>
                Number:13740200004303<br>
                Bank: Federal Bank<br>
                Branch: Pattom, Thiruvananthapuram<br>
                IFSC: FDRL0001374
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- end why section -->

<?php include('footer.php');?>