<?php include('header.php');?>
<div class="jumbotron text-center">
  <h1 class="display-3">Thank You!</h1>
  <p class="lead"><strong>You have Successfully Submitted the Entry, Please follow our  <a href="https://www.facebook.com/AmuseumArtscience">Facebook page </a>for Further Updates</strong></p>
  <p class="lead"><strong>We wish you all the very best.</strong></p>
  <hr>
  <p>
    Having trouble? <a href="http://amuseum.org.in/contact-us.php">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="index.php" role="button">Continue to homepage</a>
  </p>
</div>
<?php include('footer.php');?>