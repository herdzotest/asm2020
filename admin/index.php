<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once "database.php";
$username_err = $password_err = "";
if($_SERVER["REQUEST_METHOD"] == "POST"){
    if($_POST["username"] == ''){
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    if($_POST["password"] == ''){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
        $password_str = md5($password);
    }
    if(empty($username_err) && empty($password_err)){
        $sql = "select * from admin where user_name = '$username' and password = '$password_str'";  
        $result = mysqli_query($mysqli, $sql);  
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);  
        $count = mysqli_num_rows($result);  
          
        if($count == 1){  
            session_start();
            $_SESSION["loggedin"] = "yes";
            header("Location: list.php");
        }  
        else{  
            echo "<h1> Login failed. Invalid username or password.</h1>";  
        }     
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Amuseum | Admin </title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="style.css" rel="stylesheet" type="text/css" media="all" />
<link href='//fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="apple-touch-icon" href="assets/img/fav-icon.png">
<link rel="shortcut icon" href="assets/img/fav-icon.png">
</head>
<body>
	<!-- main -->
		<div class="main" style="background:url(assets/img/banner.jpg) 0px 0px;">
			<h1>Admin Login </h1>
			<div class="main-info">
				<div class="main-pos">
					<span  style="background:url(assets/img/1.png) no-repeat 12px 0px;"></span>
				</div>
				<div class="main-info1">
					<h3>Sign In</h3>
					<form action="index.php" method="post">
						<input type="text" name="username" placeholder="Username" required=" ">
                        <span><?php echo $username_err; ?></span>
						<input type="password" name="password" placeholder="Password" required=" ">
                        <span><?php echo $password_err; ?></span>
						<input type="submit" value="Login">
					</form>
					
				</div>
			</div>
			
			
		</div>
	<!-- //main -->
</body>
</html>