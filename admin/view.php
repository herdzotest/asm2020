<?php
include('header.php');
include 'database.php';
?>
<div style="width: 100%;min-height: 500px;background: #fff;height: auto;padding: 20px;">
<h3>Student View</h3>
<br />
<?php
$id  = $_GET['id'];
$sql = "select * from student_register inner join country on country=country_id where id=$id";
$query = mysqli_query($mysqli,$sql);
while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)){
?>
<table class="table table-bordered">
        <tr>
            <td>Name</td>
            <td><?php echo $row['name']; ?></td>
            <td>Category</td>
            <td><?php echo $row['category']; ?></td>
        </tr>
        <tr>
            <td>School</td>
            <td><?php echo $row['school']; ?></td>
            <td>Postel Address</td>
            <td><?php echo $row['postal_address']; ?></td>
        </tr>
        <tr>
            <td>Country</td>
            <td><?php echo $row['country_name']; ?></td>
            <td>Parent Name</td>
            <td><?php echo $row['parent_name']; ?></td>
        </tr>
        <tr> 
            <td>Mobile</td>
            <td><?php echo $row['mobile']; ?></td>
            <td>Email ID</td>
            <td><?php echo $row['email_id']; ?></td>
        </tr>
        <tr>
            <td>Bio Data</td>
            <td colspan="2"><?php echo $row['bio_data']; ?></td>
        </tr>
        <tr>
            <td>Transaction ID</td>
            <td><?php echo $row['transaction_id']; ?></td>
            <td>Transaction Status</td>
            <td><?php echo $row['transaction_status']; ?></td>
        </tr>  
</table>
<table class="table table-bordered">
    <tr>
        <td>Art Image</td>
        <td>Image Title</td>
        <td>Year</td>
    </tr>
    <tr>
        <td><a href="images/student/<?php echo $row['student_image']; ?>" target="_blank"><img src="images/student/<?php echo $row['student_image']; ?>" width="30px" height="30px"></a></td>
        <td><?php echo $row['image_title']; ?></td>
        <td><?php echo $row['image_year']; ?></td>
    </tr>
    <?php if($row['student_image_sec'] != '') { ?>
    <tr>
        <td><a href="images/student/<?php echo $row['student_image_sec']; ?>" target="_blank"><img src="images/student/<?php echo $row['student_image_sec']; ?>" width="30px" height="30px"></a></td>
        <td><?php echo $row['image_title_sec']; ?></td>
        <td><?php echo $row['image_year_sec']; ?></td>
    </tr>
    <?php } ?>
    <?php if($row['student_image_thr'] != '') { ?>
    <tr>
        <td><a href="images/student/<?php echo $row['student_image_thr']; ?>" target="_blank"><img src="images/student/<?php echo $row['student_image_thr']; ?>" width="30px" height="30px"></a></td>
        <td><?php echo $row['image_title_thr']; ?></td>
        <td><?php echo $row['image_year_thr']; ?></td>
    </tr>
    <?php } ?>
</table>
<?php
}
?>
</div>
<?php
include('footer.php');
?>