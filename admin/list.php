<?php
include('header.php');
include 'database.php';
$mobile = $_GET['mobile'];
$email  = $_GET['email'];
$transaction_id  = $_GET['transaction_id'];
if ($_SESSION['loggedin'] == 'yes'){
?>
<div style="width: 100%;min-height: 500px;background: #fff;height: auto;padding: 20px;">
<h3>Student List</h3>
<br />
<form method="get" action="list.php">
<div class="row">
        <div class="col-md-3 form-group">
            <input type="text" name="mobile"  placeholder="Mobile no" value="<?php echo $mobile; ?>" class="form-control"/>
        </div>
        <div class="col-md-3 form-group">
            <input type="text" name="email" placeholder="Email ID" value="<?php echo $email; ?>"  class="form-control"/>
        </div>
        <div class="col-md-3 form-group">
            <input type="text" name="transaction_id" placeholder="Transaction Id" value="<?php echo $transaction_id; ?>"  class="form-control"/>
        </div>
        <div class="col-md-3 form-group">
      	  <button type="submit" class="btn" name="student_register">Search</button>
      	</div>
       </div>
</form>
<div class="row">
<div class="col-md-2 form-group"><p><a class="dataExport" data-type="excel">Export to Excel</a></p></div>
<div class="col-md-3 form-group"><p><a class="triggerPopupBox">Bulk Download Art Images</a></p></div>
</div>
<table id="dataTable" class="table table-bordered">
    <thead>
        <tr>
            <th>S.No</th>
            <th>Name</th>
            <th>Category</th>
            <th>Mobile No</th>
            <th>Email</th>
            <th>Transaction Id</th>
            <th>Transaction Status</th>
            <th>Download Image</th>
            <!--<th>Download Id Card</th>-->
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $sno = 1;
    
    $con = '1=1';
    if($mobile !=''){
        $con.=" AND mobile LIKE '%$mobile%'";
    }
    if($email !=''){
        $con.=" AND email_id LIKE '%$email%'";
    }
    if($transaction_id !=''){
        $con.=" AND transaction_id LIKE '%$transaction_id%'";
    }
    $sql = "select * from student_register where $con";
    $query = mysqli_query($mysqli,$sql);
    while($row = mysqli_fetch_array($query, MYSQLI_ASSOC))
    {
    ?>
        <tr>
            <?php 
            if($row["class"] > 8){
                $st_cat = 'senior';
            } else {
                $st_cat = 'junior';
            }
            $st_folder = $st_cat.'/'.$row['id'].'_'.$row['name']; 
            ?>
            <td><?php echo $sno;  ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row["category"]; ?></td>
            <td><?php echo $row["mobile"]; ?></td>
            <td><?php echo $row["email_id"]; ?></td>
            <td><?php echo $row["transaction_id"]; ?></td>
            <td><?php echo $row["transaction_status"]; ?></td>
            <td><?php if($row["student_image"] !='' || $row["student_image_sec"] !='' || $row["student_image_thr"] !=''){ ?><a href="artdownload.php?art=<?php echo $st_folder;?>">Download</a><?php } ?></td>
            <td><a href="view.php?id=<?php echo $row['id']; ?>">View</a></td>
        </tr>
        
    <?php
    $sno++;
    }
    ?>
    </tbody>
</table>
</div>

<?php
}else{
    header("Location: index.php");
}
include('footer.php');
?>
<script type="text/javascript" src="assets/js/bulk/jquery.min.js"></script>
<script src="assets/js/bulk/jquery-ui.js" type="text/javascript"></script>
<link href="assets/js/bulk/jquery-ui.css" rel="stylesheet" type="text/css" />   
<script type="text/javascript">
$(document).ready(function() {
    $(".triggerPopupBox").click(function() {
        $("#dialog").dialog({
            modal: true,
            title: "Confirmation",
            width: 350,
            height: 160,
            buttons: [
            {
                id: "Junior",
                text: "Junior",
                click: function () {
                    window.location.href = "bulkdownload.php?category=junior";
                    $(this).dialog('close');
                }
            },
            {
                id: "Senior",
                text: "Senior",
                click: function () {
                    window.location.href = "bulkdownload.php?category=senior";
                    $(this).dialog('close');
                }
            },
            {
                id: "Cancel",
                text: "Cancel",
                click: function () {
                    $(this).dialog('close');
                }
            }
            ]
        });
  
    }); 

});

</script>
<div id="dialog" style="display: none; margin-bottom: -25px;margin-top: 25px;" align="center">
    Select category to download art images
</div>

<script src="assets/tableExport/tableExport.js"></script>
<script src="assets/tableExport/jquery.base64.js"></script>
<script src="assets/js/tableExport/export.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".dataExport").click(function() { 
        var exportType = $(this).data('type');      
        $('#dataTable').tableExport({
            type : exportType,          
            escape : 'toescape',
            ignoreColumn: [7,8]
        });     
    });

});
</script>
   
