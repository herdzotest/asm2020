<?php
include('database.php');
error_reporting(0);
session_start();
$logged_in = $_SESSION["loggedin"];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Amuseum | Student List</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" href="assets/img/fav-icon.png">
    <link rel="shortcut icon" href="assets/img/fav-icon.png">

    <link href='http://fonts.googleapis.com/css?family=RobotoDraft:300,400,400italic,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>



    <link href="assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"> <!-- Font Awesome -->
    <link href="assets/css/styles.css" type="text/css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" type="text/css" rel="stylesheet"> <!-- Core CSS with all styles -->

    <link href="assets/plugins/jstree/dist/themes/avenger/style.min.css" type="text/css" rel="stylesheet"> <!-- jsTree -->
    <link href="assets/plugins/codeprettifier/prettify.css" type="text/css" rel="stylesheet"> <!-- Code Prettifier -->
    <link href="assets/plugins/iCheck/skins/minimal/blue.css" type="text/css" rel="stylesheet"> <!-- iCheck -->
</head>

<body class="infobar-offcanvas" style="overflow-x: hidden;">


    <header id="topnav" class="navbar navbar-midnightblue navbar-fixed-top clearfix" role="banner">

        <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
            <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
        </span>

        <a class="navbar-brand" href="index.php">
            <h3 style="color:#fff !important;margin-top:-3%;">Amuseum Art Science</h3>
        </a>

        <span id="trigger-infobar" class="toolbar-trigger toolbar-icon-bg">
            <!-- <a data-toggle="tooltips" data-placement="left" title="Toggle Infobar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a> -->
        </span>




    </header>

    <div id="wrapper">
        <div id="layout-static">
            <!-- Left Side Bar-->
            <div class="static-sidebar-wrapper sidebar-midnightblue">
                <div class="static-sidebar">
                    <div class="sidebar">
                        <div class="widget stay-on-collapse" id="widget-welcomebox">
                            <div class="widget-body welcome-box tabular">
                                <div class="tabular-row">
                                    <div class="tabular-cell welcome-avatar">
                                        <a href="#"></a>
                                    </div>
                                    <div class="tabular-cell welcome-options">
                                        <span class="welcome-text">Welcome</span>
                                        <a href="#" class="name"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget stay-on-collapse" id="widget-sidebar">
                            <nav role="navigation" class="widget-body">
                                <ul class="acc-menu">
                                    <li class="nav-separator">Explore</li>

                                    <!-- <li><a href="register.php"><i class="fa fa-home"></i><span>Register</span></a></li> -->
                                    <?php
                                    if ($logged_in == 'yes') {
                                    ?>
                                        <li><a href="list.php"><i class="fa fa-home"></i><span>Student List</span></a></li>
                                        <li><a href="logout.php"><i class="fa fa-home"></i><span>Logout</span></a></li>
                                    <?php
                                    } else {
                                    ?>
                                        <!-- <li><a href="login.php"><i class="fa fa-home"></i><span>Admin Login</span></a></li> -->
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Left Side Bar-->