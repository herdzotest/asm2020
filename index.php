<?php include('header.php'); ?>
<!-- slider section -->
<section class="slider_section ">
  <div id="customCarousel1" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="box">
          <div class="detail_box">
            <h1>
              Thought for <br />
              Art Science
            </h1>
          </div>
          <div class="img-box">
            <img src="images/asm/sloiders1.jpg" alt="sloiders1" />
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="box">
          <div class="detail_box">
            <h1>
              Art Meet<br />
              Science
            </h1>
          </div>
          <div class="img-box">
            <img src="images/asm/sloiders4.jpg" alt="sloiders4.jpg" />
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="box">
          <div class="detail_box">
            <h1>
              tellMed<br />
              Amuseum
            </h1>
          </div>
          <div class="img-box">
            <img src="images/asm/tellMEd_logo_eng.png" alt="tellMEd_logo_eng.png" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="carousel_btn-box">
    <a class="carousel-control-prev" href="#customCarousel1" role="button" data-slide="prev">
      <i class="fa fa-arrow-left" aria-hidden="true"></i>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#customCarousel1" role="button" data-slide="next">
      <i class="fa fa-arrow-right" aria-hidden="true"></i>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section>
<!-- end slider section -->
</div>

<!-- about section -->

<section class="about_section about_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="detail-box">
          <div class="heading_container"><br><br><br>
            <h2><br><br>
              About Amuseum
            </h2>
          </div>
          <p>
          ‘Amuseum is a new age concept foundation for Artscience Philosophy established in Trivandrum, the capital city of Kerala, India. Amuseum is a collective of independent intellectuals, artists, scientists and cultural enthusiasts from different parts of the Globe. It aims to emphasis the value of interdisciplinary inquiry bringing together Art and Science in experiencing and understanding the world around us in a broader and deeper way.
          </p>
          <a href="about.php">
            Read More
          </a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img-box"><br>
          <img src="images/asm/amuseum in roundA low.png" alt="amuseum in roundA low.png" width="300" height="300"/>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- end about section -->

<section class="tellmed_section layout_padding tellmed_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="img-box">
        <br><br><img src="images/asm/tellmed_logo_03.png" alt="tellmed_logo_03.png" width="400" height="200" />
        </div>
      </div>
      <div class="col-md-6">
        <div class="detail-box">
          <div class="heading_container">
            <h2>
            TellMED
            </h2>
          </div>
          <p>
          ‘A call away’<br>
          Free Telemedicine support to India<br>
          </p>
          <p>
          Connect to tellMED<br>
          Call: <b>85890 61461</b><br>
          <b>9995036666</b><br>
          </p>
          <p>
          Alternatively Join<br>
          <b>www.covid.unarv.com</b><br>
          Password:10<br>
          Or<br>
          Zoom Webinar ID: <b>863 4365 0001</b><br>
          Please click the link below to join the webinar:<br>
          <b><a href="https://us02web.zoom.us/j/86343650001">https://us02web.zoom.us/j/86343650001</a></b>
          </p>
          <a href="tellmed.php" style="margin-top: 25px;display: inline-block;padding: 10px 45px;background-color: #000000;color: #ffffff;text-transform: uppercase;transition: all .3s;">
            Read More
          </a><br><br><br>
        </div>
      </div>
    </div>
    <h2>
      AMUSEUM STUDENT ART PRIZE 2020 - WINNERS<br><br>
    </h2>
    <h4>WINNERS - JUNIOR</h4>
    <div class="row">
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_winner_01.jpeg" alt="junior_winner_01.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_winner_02.jpeg" alt="junior_winner_02.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_winner_03.jpeg" alt="junior_winner_03.jpeg" width="360" height="250" />
        </div>
      </div>
    </div><br><br>
    <h4>WINNERS - SENIOR</h4>
      <div class="row">
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_winner_01.jpeg" alt="senior_winner_01.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_winner_02.jpeg" alt="senior_winner_02.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_winner_03.jpeg" alt="senior_winner_03.jpeg" width="360" height="250" />
        </div>
      </div>
    </div>

  </div>
</section>



<?php include('footer.php'); ?>