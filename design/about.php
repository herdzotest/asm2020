<?php include('header.php'); ?>
<!-- about section -->

<section class="about_section layout_padding about_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="detail-box">
          <div class="heading_container">
            <h2>
              About Amuseum
            </h2>
          </div>
          <p>
            ‘Amuseum Thought for ArtScience’ is a new age concept foundation established in Trivandrum, the capital city of Kerala, India. Amuseum aims to create a knowledge pool based on the convergence of art and science creativity which often deemed as never meeting parallels. In this de-curative space research meets creativity and together they build an ever growing cultural archives of artscience practices.

            ‘Amuseum ArtScience’, a new age concept foundation bringing art and science together to build an ever growing cultural archives of creativity.

            “Meanwhile, because humanity is still swept along by animal passions in a digitalized global world, and because we are conflicted between what we are and what we wish to become, and because we are drowning in information and starved for wisdom, it would seem appropriate to return philosophy to its once esteemed position, this time as the center of a humanistic science and a scientific humanities…. Scientists and scholars in the humanities, working together, will, serve as the leaders of a new philosophy, one that blends the best and most relevant from these two great branches of learning. Their effort will be the third Enlightenment. Unlike the first two, this one might well endure.”
            ― Edward O. Wilson, The Origins of Creativity
          </p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img-box">
          <img src="images/asm/amuseum in roundA low.png" alt="amuseum in roundA low.png">
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php'); ?>