<?php include('header.php'); ?>
<!-- slider section -->
<section class="slider_section ">
  <div id="customCarousel1" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="box">
          <div class="detail_box">
            <h1>
              Thought for <br />
              Art Science
            </h1>
          </div>
          <div class="img-box">
            <img src="images/asm/sloiders1.jpg" alt="sloiders1" />
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="box">
          <div class="detail_box">
            <h1>
              Art Meet<br />
              Science
            </h1>
          </div>
          <div class="img-box">
            <img src="images/asm/sloiders4.jpg" alt="sloiders4.jpg" />
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="box">
          <div class="detail_box">
            <h1>
            Amuseum International<br />
            Student Art Prize 2020
            </h1>
          </div>
          <div class="img-box">
            <img src="images/asm/sloiders2.jpg" alt="sloiders2.jpg" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="carousel_btn-box">
    <a class="carousel-control-prev" href="#customCarousel1" role="button" data-slide="prev">
      <i class="fa fa-arrow-left" aria-hidden="true"></i>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#customCarousel1" role="button" data-slide="next">
      <i class="fa fa-arrow-right" aria-hidden="true"></i>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section>
<!-- end slider section -->
</div>

<!-- about section -->

<section class="about_section layout_padding about_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="detail-box">
          <div class="heading_container">
            <h2>
              About Amuseum
            </h2>
          </div>
          <p>
            ‘Amuseum Thought for ArtScience’ is a new age concept foundation
            established in Trivandrum, the capital city of Kerala, India.
            Amuseum aims to create a knowledge pool based on the convergence
            of art and science creativity which often deemed as never
            meeting parallels. In this de-curative space research meets
            creativity and together they build an ever growing cultural
            archives of artscience practices.
          </p>
          <a href="about.html">
            Read More
          </a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img-box">
          <img src="images/asm/amuseum in roundA low.png" alt="amuseum in roundA low.png" />
        </div>
      </div>
    </div>
  </div>
</section>

<!-- end about section -->

<!-- flower section -->

<section class="flower_section  about_flower_common_section layout_padding-bottom">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="img-box">
          <img src="images/asm/artprize logo 2.png" alt="artprize logo 2.png" />
        </div>
      </div>
      <div class="col-md-6">
        <div class="detail-box">
          <div class="heading_container">
            <h2>
              Amuseum Student ART Prize 2020
            </h2>
          </div>
          <p>
            International Competition on Art for School Students)<br />
            (Open to ALL School students) <br />
            Last Date to apply: <strong> 10 December 2020</strong> <br />
            Apply with three artworks of Drawing, Painting or Collage with
            any media created during the Covid-19 Pandemic Period <br />
            <strong> Topic: Locked-in-Home </strong> <br />
            <a href="Amuseum-Student-ART-Prize-2020.html">
              Apply Now
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- end flower section -->

<!-- why section -->

<section class="why_section layout_padding">
  <div class="container">
    <div class="heading_container">
      <h2>
        Win Exciting Prices
      </h2>
      <p>
        The winners will get an exciting cash prize for their arts.
      </p>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="box pr-0 pr-lg-5">
          <div class="img-box">
            <img src="images/asm/winners logo 2.png" alt="winners logo 2.png" />
          </div>
          <div class="detail-box">
            <h5>
              Prices for Senior Category
            </h5>
            <ul>
              <li>
                Black Prize: 20000
              </li>
              <li>
                Yellow Prize:10000
              </li>
              <li>
                Magenta Prize: 5000
              </li>
              <li>
                Cyan Prizes: 2000 Rs prize for 10 particip
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box pr-0 pr-lg-5">
          <div class="img-box">
            <img src="images/asm/winners logo 1.png" alt="winners logo 1.png" />
          </div>
          <div class="detail-box">
            <h5>
              Price for Junior Category
            </h5>
            <ul>
              <li>
                Black Prize: 10000
              </li>
              <li>
                Yellow Prize:5000
              </li>
              <li>
                Magenta Prize: 3000
              </li>
              <li>
                Cyan Prizes: 1000 Rs prize for 10 particip
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- end why section -->

<!-- client section -->

<!-- <section class="client_section layout_padding-bottom">
      <div class="container">
        <div class="heading_container">
          <h2>
            Greetings from Legends
          </h2>
        </div>
      </div>
      <div id="customCarousel2" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="container">
              <div class="col-md-10 mx-auto">
                <div class="box">
                  <div class="img-box">
                    <img src="images/client.png" alt="" />
                  </div>
                  <div class="detail-box">
                    <h5>
                      kk Morch
                    </h5>
                    <p>
                      If you are going to use a passage of Lorem Ipsum, you need
                      to be sure there isn't anything embarrassing hidden in the
                      middle of text. All the Lorem Ipsum generators on the
                      Internet tend to repeat predefined
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container">
              <div class="col-md-10 mx-auto">
                <div class="box">
                  <div class="img-box">
                    <img src="images/client.png" alt="" />
                  </div>
                  <div class="detail-box">
                    <h5>
                      kk Morch
                    </h5>
                    <p>
                      If you are going to use a passage of Lorem Ipsum, you need
                      to be sure there isn't anything embarrassing hidden in the
                      middle of text. All the Lorem Ipsum generators on the
                      Internet tend to repeat predefined
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container">
              <div class="col-md-10 mx-auto">
                <div class="box">
                  <div class="img-box">
                    <img src="images/client.png" alt="" />
                  </div>
                  <div class="detail-box">
                    <h5>
                      kk Morch
                    </h5>
                    <p>
                      If you are going to use a passage of Lorem Ipsum, you need
                      to be sure there isn't anything embarrassing hidden in the
                      middle of text. All the Lorem Ipsum generators on the
                      Internet tend to repeat predefined
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ol class="carousel-indicators">
          <li
            data-target="#customCarousel2"
            data-slide-to="0"
            class="active"
          ></li>
          <li data-target="#customCarousel2" data-slide-to="1"></li>
          <li data-target="#customCarousel2" data-slide-to="2"></li>
        </ol>
      </div>
    </section> -->

<!-- end client section -->



<?php include('footer.php'); ?>