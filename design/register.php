<?php
error_reporting(0);
include('header.php');
$message = $_GET['message'];
?>
<div style="width: 100%;min-height: 500px;background: #fff;height: auto;padding: 20px;">
  <h3>Student Register form</h3>
  <?php
  if ($message != '') {
  ?>
    <h4 class="alert alert-success" role="alert" id="successmessage"><?php if ($message != '') {
                                                                        echo $message;
                                                                      } ?></h4>
  <?php
  }
  ?>
  <br />
  <form method="post" action="register_form_submit.php" id="register_form_submit" enctype="multipart/form-data">
    <input type="hidden" name="action" value="register" />
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Student Name</label>
        <input type="text" name="name" id="student_name" class="form-control" value="" />
        <span id="student_name_info" style="color: red;"></span>
      </div>
      <div class="col-md-4 form-group">
        <label>Class</label>
        <select name="class" id="myclass" class="form-control" onchange="setCategory();">
          <?php
          for ($i = 1; $i <= 12; $i++) {
          ?>
            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
          <?php
          }
          ?>
        </select>
      </div>
      <div class="col-md-4 form-group">
        <label>Category</label>
        <input type="text" name="category" id="category" value="Junior" class="form-control" readonly="true">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 form-group">
        <label>School</label>
        <input type="text" name="school" class="form-control">
      </div>
      <div class="col-md-4 form-group">
        <label>Nationality</label>
        <input type="text" name="nationality" class="form-control" />
      </div>
      <div class="col-md-4 form-group">
        <label>Postel Address</label>
        <textarea name="postal" class="form-control"></textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Country</label>
        <select name="country" class="form-control">
          <?php
          $sql = "select * from country";
          $query = mysqli_query($mysqli, $sql);
          while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
          ?>
            <option value="<?php echo $row['country_id']; ?>"><?php echo $row['country_name']; ?></option>
          <?php
          }

          ?>
        </select>
      </div>
      <div class="col-md-4 form-group">
        <label>Parent Name</label>
        <input type="text" name="parent_name" id="parent_name" class="form-control">
        <span id="parent_name_info" style="color : red;"></span>
      </div>
      <div class="col-md-4 form-group">
        <label>Mobile</label>
        <input type="text" name="mobile_no" class="form-control allow_number_only">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Whats app Number</label>
        <input type="text" name="whatsapp" class="form-control allow_number_only">
      </div>
      <div class="col-md-4 form-group">
        <label>Email ID</label>
        <input type="text" name="email_id" id="email" class="form-control" value="">
        <span id="email_info" style="color: red;"></span>
      </div>
      <div class="col-md-4 form-group">
        <label>Student Id Card</label>
        <input type="file" name="student_document" accept="image/jpg, image/jpeg, image/png" />
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 form-group">
        <label>Bio Data</label>
        <textarea name="bio_data" id="bio_data" class="form-control"></textarea>
        <span id="bio_data_character_count" style="color: red;"></span>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 form-group" id="st-img-fst" style="display: inline;">
        <label>Art Image 1</label>
        <input type="file" name="student_image" id="student_image" onChange="fileSelect();" accept="image/jpg, image/jpeg, image/png" />
      </div>
    </div>
    <div class="row" id="st-img-div">
      <div class="col-md-3 form-group" id="st-title1" style="display: none;">
        <label>Image Title</label>
        <input type="text" name="image_title" id="image_title" class="form-control">
        <span id="image_title_info" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-size1" style="display: none;">
        <label>Size in cm</label>
        <input type="text" name="image_size" id="image_size" class="form-control">
        <span id="image_size_info" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-medium1" style="display: none;">
        <label>Medium</label>
        <input type="text" name="image_medium" id="image_medium" class="form-control">
        <span id="image_medium_info" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-year1" style="display: none;">
        <label>Year</label>
        <select name="image_year" id="image_year" class="form-control">
          <?php
          $years = range(date('Y'), 2000);
          foreach ($years as $key => $value) { ?>
            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="input-group" id="st-btn-sec" style="display: none;">
      <p class="button-reg" onClick="stImage();">Add second image</p>
    </div>
    <br /><br />
    <div class="row">
      <div class="col-md-4 form-group" id="st-img-sec" style="display: none;">
        <label>Art Image 2</label>
        <input type="file" name="student_image_sec" id="student_image_sec" onChange="fileSelectSec();" accept="image/jpg, image/jpeg, image/png" />
      </div>
    </div>
    <div class="row" id="st-img-div-sec">
      <div class="col-md-3 form-group" id="st-title2" style="display: none;">
        <label>Image Title</label>
        <input type="text" name="image_title_sec" id="image_title_sec" class="form-control">
        <span id="image_title_info_sec" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-size2" style="display: none;">
        <label>Size in cm</label>
        <input type="text" name="image_size_sec" id="image_size_sec" class="form-control">
        <span id="image_size_info_sec" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-medium2" style="display: none;">
        <label>Medium</label>
        <input type="text" name="image_medium_sec" id="image_medium_sec" class="form-control">
        <span id="image_medium_info_sec" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-year2" style="display: none;">
        <label>Year</label>
        <select name="image_year_sec" id="image_year_sec" class="form-control">
          <?php
          $years = range(date('Y'), 2000);
          foreach ($years as $key => $value) { ?>
            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="input-group" id="st-btn-thr" style="display: none;">
      <p class="button-reg" onClick="stImageThr();">Add third image</p>
    </div>
    <br /><br />

    <div class="row">
      <div class="col-md-4 form-group" id="st-img-thr" style="display: none;">
        <label>Art Image 3</label>
        <input type="file" name="student_image_thr" onChange="fileSelectThr();" accept="image/jpg, image/jpeg, image/png" />
      </div>
    </div>
    <div class="row" id="st-img-div-thr">
      <div class="col-md-3 form-group" id="st-title3" style="display: none;">
        <label>Image Title</label>
        <input type="text" name="image_title_thr" id="image_title_thr" class="form-control">
        <span id="image_title_info_thr" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-size3" style="display: none;">
        <label>Size in cm</label>
        <input type="text" name="image_size_thr" id="image_size_thr" class="form-control">
        <span id="image_size_info_thr" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-medium3" style="display: none;">
        <label>Medium</label>
        <input type="text" name="image_medium_thr" id="image_medium_thr" class="form-control">
        <span id="image_medium_info_thr" style="color: red;"></span>
      </div>
      <div class="col-md-3 form-group" id="st-year3" style="display: none;">
        <label>Year</label>
        <select name="image_year_thr" id="image_year_thr" class="form-control">
          <?php
          $years = range(date('Y'), 2000);
          foreach ($years as $key => $value) { ?>
            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="row">
      <div class="col-md-2 form-group" id="qr_code" style="display: none;">
        <label></label>
        <img src="images/qr_code.png" width="100px" height="100px"><br>
        <span>Scan QR code to pay</span>
      </div>
      <div class="col-md-4 form-group" id="transaction" style="display: none;">
        <label>Transaction ID</label>
        <input type="text" name="transaction_id" id="transaction_id" onChange="transactionFunction();" class="form-control">
        <span id="transaction_id_info" style="color : red;"></span>
      </div>
    </div>

    <div class="input-group">
      <button type="submit" class="btn btn-primary" name="student_register" id="student_register" style="display: none;">Register</button>
    </div>
    <br /><br />
  </form>

</div>
<?php
include('footer.php');
?>
<style type="text/css">
  .button-reg {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 7px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 14px;
    margin: 4px 2px;
    border-radius: 4px;
    cursor: pointer;
  }
</style>

<script type="text/javascript">
  function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
      return false;
    } else {
      return true;
    }
  }

  function setCategory() {
    var x = document.getElementById("myclass").value;
    if (x > 8) {
      document.getElementById("category").value = "Senior";
    } else {
      document.getElementById("category").value = "Junior";
    }
  }

  function fileSelect() {
    document.getElementById("st-title1").style.display = "inline";
    document.getElementById("st-size1").style.display = "inline";
    document.getElementById("st-medium1").style.display = "inline";
    document.getElementById("st-year1").style.display = "inline";
    document.getElementById("st-btn-sec").style.display = "inline";
  }

  function stImage() {
    let error = 0;
    var image_title = $("#image_title").val();
    if (image_title == '') {
      $("#image_title_info").html("Image title can't be empty");
      error = 1;
    }
    var image_size = $("#image_size").val();
    if (image_size == '') {
      $("#image_size_info").html("Size can't be empty");
      error = 1;
    }
    var image_medium = $("#image_medium").val();
    if (image_medium == '') {
      $("#image_medium_info").html("Medium can't be empty");
      error = 1;
    }
    if (error == 1) {
      e.preventDefault();
      return false;
    } else {
      document.getElementById("st-btn-sec").style.display = "none";
      $('#student_image').attr('disabled', true);
      document.getElementById("image_title").readOnly = true;
      document.getElementById("image_size").readOnly = true;
      document.getElementById("image_medium").readOnly = true;
      $('#image_year').attr('disabled', true);
      document.getElementById("st-img-sec").style.display = "inline";
    }
  }

  function fileSelectSec() {
    document.getElementById("st-title2").style.display = "inline";
    document.getElementById("st-size2").style.display = "inline";
    document.getElementById("st-medium2").style.display = "inline";
    document.getElementById("st-year2").style.display = "inline";
    document.getElementById("st-btn-thr").style.display = "inline";
  }

  function stImageThr() {
    let error = 0;
    var image_title_sec = $("#image_title_sec").val();
    if (image_title_sec == '') {
      $("#image_title_info_sec").html("Image title can't be empty");
      error = 1;
    }
    var image_size_sec = $("#image_size_sec").val();
    if (image_size_sec == '') {
      $("#image_size_info_sec").html("Size can't be empty");
      error = 1;
    }
    var image_medium_sec = $("#image_medium_sec").val();
    if (image_medium_sec == '') {
      $("#image_medium_info_sec").html("Medium can't be empty");
      error = 1;
    }
    if (error == 1) {
      e.preventDefault();
      return false;
    } else {
      document.getElementById("st-btn-thr").style.display = "none";
      $('#student_image').attr('disabled', true);
      document.getElementById("image_title").readOnly = true;
      document.getElementById("image_size").readOnly = true;
      document.getElementById("image_medium").readOnly = true;
      $('#image_year').attr('disabled', true);
      $('#student_image_sec').attr('disabled', true);
      document.getElementById("image_title_sec").readOnly = true;
      document.getElementById("image_size_sec").readOnly = true;
      document.getElementById("image_medium_sec").readOnly = true;
      $('#image_year_sec').attr('disabled', true);
      document.getElementById("st-img-thr").style.display = "inline";
    }
  }

  function fileSelectThr() {
    document.getElementById("st-title3").style.display = "inline";
    document.getElementById("st-size3").style.display = "inline";
    document.getElementById("st-medium3").style.display = "inline";
    document.getElementById("st-year3").style.display = "inline";
    //document.getElementById("student_register").style.display = "inline";
    document.getElementById("qr_code").style.display = "inline";
    document.getElementById("transaction").style.display = "inline";
  }

  function transactionFunction() {
    document.getElementById("student_register").style.display = "inline";
  }
  $(document).ready(function() {
    $('.character_only').bind('keyup blur', function() {
      var current_txt_box = $(this);
      current_txt_box.val(current_txt_box.val().replace(/[^a-z]/g, ''));
    });
    $(".allow_number_only").on("keypress keyup blur", function(event) {
      $(this).val($(this).val().replace(/[^\d].+/, ""));
      if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
    $('#bio_data').keypress(function(e) {
      var tval = $('#bio_data').val(),
        tlength = tval.length,
        set = 500,
        remain = parseInt(set - tlength);
      $('#bio_data_character_count').text("Remaining Character : " + remain);
      if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
        $('#bio_data').val((tval).substring(0, tlength - 1));
        return false;
      }
    });
    $("#register_form_submit").submit(function(e) {
      let error = 0;
      var student_name = $("#student_name").val();
      if (student_name == '') {
        $("#student_name_info").html("Student name can't be empty");
        error = 1;
      }
      var parent_name = $("#parent_name").val();
      if (parent_name == '') {
        $("#parent_name_info").html("Parent name can't be empty");
        error = 1;
      }
      var image_title = $("#image_title").val();
      if (image_title == '') {
        $("#image_title_info").html("Image title can't be empty");
        error = 1;
      }
      var image_size = $("#image_size").val();
      if (image_size == '') {
        $("#image_size_info").html("Size can't be empty");
        error = 1;
      }
      var image_medium = $("#image_medium").val();
      if (image_medium == '') {
        $("#image_medium_info").html("Medium can't be empty");
        error = 1;
      }
      var image_title_sec = $("#image_title_sec").val();
      if (image_title_sec == '') {
        $("#image_title_info_sec").html("Image title can't be empty");
        error = 1;
      }
      var image_size_sec = $("#image_size_sec").val();
      if (image_size_sec == '') {
        $("#image_size_info_sec").html("Size can't be empty");
        error = 1;
      }
      var image_medium_sec = $("#image_medium_sec").val();
      if (image_medium_sec == '') {
        $("#image_medium_info_sec").html("Medium can't be empty");
        error = 1;
      }
      var image_title_thr = $("#image_title_thr").val();
      if (image_title_thr == '') {
        $("#image_title_info_thr").html("Image title can't be empty");
        error = 1;
      }
      var image_size_thr = $("#image_size_thr").val();
      if (image_size_thr == '') {
        $("#image_size_info_thr").html("Size can't be empty");
        error = 1;
      }
      var image_medium_thr = $("#image_medium_thr").val();
      if (image_medium_thr == '') {
        $("#image_medium_info_thr").html("Medium can't be empty");
        error = 1;
      }
      var transaction_id = $("#transaction_id").val();
      if (transaction_id == '') {
        $("#transaction_id_info").html("Transaction id can't be empty");
        error = 1;
      }
      var email = $('#email').val();
      if (IsEmail(email) == false) {
        $('#email_info').html('Invalid Email ID');
        error = 1;
      } else {
        $('#email_info').html('');
      }
      if (error == 1) {
        e.preventDefault();
        return false;
      } else {
        $('#student_image').attr('disabled', false);
        $('#student_image_sec').attr('disabled', false);
        $('#image_year').attr('disabled', false);
        $('#image_year_sec').attr('disabled', false);
        return true;
      }
    });
    setTimeout(function() {
      $('#successmessage').html('');
      $('#successmessage').removeClass('alert-success');
      $('#successmessage').removeClass('alert');
    }, 700);
  });
</script>