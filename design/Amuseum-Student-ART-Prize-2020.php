<?php include('header.php');?>

    <section class="flower_section  about_flower_common_section layout_padding">
      <div class="container  ">
        <div class="row">
          <div class="col-md-12">
            <div class="detail-box">
              <div class="heading_container">
                <h2>
                  Amuseum Student ART Prize 2020
                </h2>
              </div>
              <div class="img-box" style="text-align: center;">
                <img src="images/asm/student artprize image.png" alt="student artprize image.png" />
              </div>
              <p>
                Amuseum Student Art Prize 2020 is an initiative by Amuseum
                ArtScience, a new age experimental platform registered as a
                Trust that aspires to bring together the concepts of both art
                and science on a single platform in order to see the world more
                holistically. As a part of the emerging concept of ‘ArtScience’
                all over the world, Amuseum ArtScience has initiated this
                Student Art Prize 2020 in order to engage the school children
                with the ideas of art and science through the medium of art. The
                world has been going through a pandemic and ‘lockdown’ and
                ‘quarantine’ have become household words and concepts. As fast
                adapters of emerging tendencies and trends, children too have
                adjusted themselves with the new situation while engaging with
                the world through various mediums of which internet enabled
                digital platforms play a major role. It is not that they have
                not been doing anything in real time and in real space. Their
                offline activities have resulted into art, journal entries,
                poems, short stories, photography and even short films. Amuseum
                Student Art Prize 2020 is an attempt to see what the children of
                the world have been doing during the last seven months mostly
                ‘locked up inside homes’.
              </p>
              <p>
                All School Students could participate in this mega art festival
                by sending in the digital/photographic images of their creative
                works such as paintings, drawings, collages and sculptures for
                the Amuseum Student Art Prize 2020.
              </p>
              <h5>
                Categories:
              </h5>
              <p>
                There are two categories in this international competition;
                junior and senior. Students upto 8 th Forum/ STD (aged upto 13
                years) belong to the Junior category. Students from 9 to 12
                Forum/ STD ( 14 years Age or above belong to the Senior
                category.
              </p>
              <h5>Theme: ‘Locked-in-Home’</h5>
              <h5>Nature of Entries:</h5>
              <p>
                Each participant should send in three images of their works (it
                could be three different paintings or drawings or collages or
                sculpture or one image from each category). Participants are
                free to choose their medium (for example, oil on canvas, acrylic
                on canvas/paper, water colours, ink drawings, paper cuttings and
                so on). However, the entries sent to Amuseum Student Art Prize
                2020 should be a digital image. Originals are not required. Each
                participant could send in images not more than five.
              </p>
              <h5>Additional Requirements:</h5>
              <p>
                Each work should have a title (eg. My Home), medium (eg. Water
                colour on paper), size (eg 25 cm x36 cm), year (2020) A scanned
                image of the School ID card Details of Remittence of the
                Registration Fee A short Bio/ Achievements
              </p>
              <h5>
                Where to send the images and how to send?
              </h5>
              <p>
                The images should be sent through the Amuseum website.
                www.amuseum.org.in. In case of any difficulty you can send the
                files along with the filled registration form to
                amuseumartscience@gmail.com All the images should be between
                100- 200 dpi resolution.
              </p>
              <h5>Last Date of Application: 20th December 2020</h5>
              <br />
              <h5>Nature of the Prize:</h5>
              <p>
                <strong> Senior Category</strong><br />
                First Prize is called ‘Black Prize’ and carries a purse of
                Indian Rupees 20,000/- and a certificate, portfolio of eminent
                works of art, and an Amuseum Souvenir.<br />
                Second Prize is called ‘Yellow Prize’ and carries a purse of
                Indian Rupees 10,000/- and a certificate, portfolio of eminent
                works of art and an Amuseum Souvenir. <br />
                Third Prize is called ‘Magenta Prize’ and carries a purse of
                Indian Rupees 5000/- and a certificate, portfolio of eminent
                works of art and an Amuseum Souvenir. <br />
                Fourth Prize is called ‘Cyan Prize’ and carries a purse of
                Indian Rupees 2000/- and a certificate, portfolio of eminent
                works of art and an Amuseum Souvenir. (Cyan Prize is given to 10
                selected participants)
                <br /><br />
                <strong> Junior Category Prizes </strong> <br />

                First Prize is called ‘Black Prize’ and carries a purse of
                Indian Rupees 10,000/- and a certificate, portfolio of eminent
                works of art, and an Amuseum Souvenir. <br />
                Second Prize is called ‘Yellow Prize’ and carries a purse of
                Indian Rupees 5000/- and a certificate, portfolio of eminent
                works of art and an Amuseum Souvenir. <br />
                Third Prize is called ‘Magenta Prize’ and carries a purse of
                Indian Rupees 3000/- and a certificate, portfolio of eminent
                works of art and an Amuseum Souvenir. <br />
                Fourth Prize is called ‘Cyan Prize’ and carries a purse of
                Indian Rupees 1000/- and a certificate, portfolio of eminent
                works of art and an Amuseum Souvenir. (Cyan Prize is given to 10
                selected participants)<br />
              </p>
              <h5>How to Register for Amuseum Student Art Prize 2020?</h5>
              <p>
                You may go through the registration link in the website
                (www.amuseum.org.in) and follow the instructions. Alternatively
                Printed Registration form can be filled and scanned to send
                through Email to amuseumartscience@gmail.com Registration Fee is
                Indian Rupees 100/- should be payed with our Bank. <br />
                Banking Details are as follows <br />
                <b>
                  Name: Amuseum Artscience <br />Account Number:13740200004303
                  <br />
                  Bank: Federal Bank Branch: Pattom, Thiruvananthapuram <br />
                  IFSC:FDRL0001374</b
                >
              </p>
              <h5>Jury Members:</h5>
              <p>
                A jury of eminent artists, scholars and educators will judge the
                works. Their names will be declared soon. The decision of the
                Jury will be final on the Awards.
              </p>
              <br />
              <h5>Date of Announcement of the Results: 1st January 2021.</h5><br>
              <h5>
                Additional Takeaways for the Participants:
              </h5>
              <p>There will be two online master classes with the duration of two
              hours each on water colour painting and Portrait painting for
              selected 500 participants. This will be held after the
              announcement of the Prizes in two consecutive weekends. 100 works
              selected by the jury will be on display as a month-long exhibition
              in the Amuseum Online Gallery and included in a limited edition
              Catalogue.</p>
              <h5>For further Details:</h5>
              <p>
                
                mail to : amuseumartscience@gmail.com <br>
                Whats app: +91 8589061461, +91 9995036666 <br>
                Phone: +91 9995036666
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php include('footer.php');?>