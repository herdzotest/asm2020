<?php include('header.php');?>

    <!-- why section -->

    <section class="why_section layout_padding">
      <div class="container">
        <div class="heading_container">
          <h2>
            Contact Us
          </h2>
          <p>
            For anything you can reach out to us, feel free to ask your doubts,
            will help you in everything for sure.
          </p>
        </div>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="box pr-0 pr-lg-5">
              <div class="img-box">
              </div>
              <div class="detail-box">
                AMUSEUM Artscience <br>
                TC 979/26 Plamoodu -PMG Highway Pattom Post.<br>
                Thiruvananthapuram. 695004. Kerala. India. <br> 
                Phone : +91 9995036666, +91 8589061461 <br>
                Email : amuseumartscience@gmail.com <br>
                Web :www.amuseum.org.in
              </div>
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </section>

    <!-- end why section -->

<?php include('footer.php');?>