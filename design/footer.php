   <!-- info section -->
<section class="info_section">
  <div class="container">
    <div class="info_contact_container">
      <h3>
        Contact Us
      </h3>
      <div class="info_contact_box">
        <p>
          AMUSEUM Artscience TC 979/26 Plamoodu -PMG Highway Pattom Post.
          Thiruvananthapuram. 695004. Kerala. India. www.amuseum.org.in
        </p>
        <a href="">
          <i class="fa fa-phone" aria-hidden="true"></i>
          <span>
            +91 9995036666, +91 8589061461
          </span>
        </a>
        <a href="">
          <i class="fa fa-envelope"></i>
          <span>
            amuseumartscience@gmail.com
          </span>
        </a>
      </div>
    </div>
    <div class="social_box">
      <a href="www.facebook.com/AmuseumArtscience">
        <i class="fa fa-facebook" aria-hidden="true"></i>
      </a>
      <a href="https://twitter.com/Amuseum6?s=09">
        <i class="fa fa-twitter" aria-hidden="true"></i>
      </a>
      <a href="https://instagram.com/amuseumartscience">
        <i class="fa fa-instagram" aria-hidden="true"></i>
      </a>
      <a href="www.Youtube.com/AmuseumArtscience">
        <i class="fa fa-youtube-play" aria-hidden="true"></i>
      </a>
    </div>
    <div class="info_link_box">
      <a href="index.php">
        Home
      </a>
      <a href="about.php">
        About
      </a>
      <a href="Amuseum-Student-ART-Prize-2020.php">
        Amuseum Student ART Prize 2020
      </a>
      <a href="register.php">
        Register
      </a>
      <a href="contact-us.php">
        Contact Us
      </a>
    </div>
  </div>
</section>

<!-- end info_section -->
   <!-- footer section -->
   <section class="container-fluid footer_section">
      <div class="container">
        <div class="col-md-11 col-lg-8 mx-auto">
          <p>
            &copy; <span id="displayYear"></span> All Rights Reserved By
            Amuseum, Powered By
            <a href="https://herdzo.com">Herdzo Innovations (P) Ltd</a>
          </p>
        </div>
      </div>
    </section>
    <!-- footer section -->

    <!-- jQery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- slick slider -->
    <script
      type="text/javascript"
      src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"
    ></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>
  </body>
</html>