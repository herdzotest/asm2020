<?php
error_reporting(0);
include 'database.php';
if($_POST['action'] == 'register'){
    $name=$_POST['name'];
    $class=$_POST['class'];
    $category=$_POST['category'];
    $school=$_POST['school'];
    $nationality=$_POST['nationality'];
    $postal_address=$_POST['postal'];
    $country=$_POST['country'];
    $parent_name=$_POST['parent_name'];
    $mobile=$_POST['mobile_no'];
    $whatsapp=$_POST['whatsapp'];
    $email = $_POST['email_id'];
    $bio_data=$_POST['bio_data'];
    $image_title = $_POST['image_title'];
    $image_size = $_POST['image_size'];
    $image_medium = $_POST['image_medium'];
    $image_year = $_POST['image_year']; 
    $image_title_sec = $_POST['image_title_sec'];
    $image_size_sec = $_POST['image_size_sec'];
    $image_medium_sec = $_POST['image_medium_sec'];
    $image_year_sec = $_POST['image_year_sec']; 
    $image_title_thr = $_POST['image_title_thr'];
    $image_size_thr = $_POST['image_size_thr'];
    $image_medium_thr = $_POST['image_medium_thr'];
    $image_year_thr = $_POST['image_year_thr'];
    $transaction_id = $_POST['transaction_id'];
    if($transaction_id != ''){
        $transaction_status = 'Paid';
    } else {
        $transaction_status = 'Pending';
    }
    $document = '';
    if($_FILES['student_document']['name']){
        move_uploaded_file($_FILES['student_document']['tmp_name'], "images/document/".$_FILES['student_document']['name']);
        $document = $_FILES['student_document']['name'];
        $document_ext = substr($document, strripos($document, '.'));
    }
    
    $insert_query ="insert into student_register(name,class,category,school,nationality,postal_address,country,parent_name,mobile,whatsapp,email_id,bio_data,document,image_title,image_size,image_medium,image_year,image_title_sec,image_size_sec,image_medium_sec,image_year_sec,image_title_thr,image_size_thr,image_medium_thr,image_year_thr,transaction_id,transaction_status)
    values('$name','$class','$category','$school','$nationality','$postal_address','$country','$parent_name','$mobile','$whatsapp','$email','$bio_data','$document','$image_title','$image_size','$image_medium','$image_year','$image_title_sec','$image_size_sec','$image_medium_sec','$image_year_sec','$image_title_thr','$image_size_thr','$image_medium_thr','$image_year_thr','$transaction_id','$transaction_status')";
    $rec = mysqli_query($mysqli, $insert_query); 
    if($rec == '1'){ 
        $last_id = $mysqli->insert_id;
        if($last_id != ''){
            if($class > 8){
                $st_cat = 'senior';
            } else {
                $st_cat = 'junior';
            }
            $st_dir = $last_id.'_'.$name;
            if (!file_exists('images/student/'.$st_cat.'/'.$st_dir)) {
                mkdir('images/student/'.$st_cat.'/'.$st_dir, 0777, true);
            }
            $art_img1 = $last_id.'_'.$name.'_1'; 
            $art_img2 = $last_id.'_'.$name.'_2'; 
            $art_img3 = $last_id.'_'.$name.'_3'; 
            if($_FILES['student_image']['name']){
                $filename1 = $_FILES["student_image"]["name"];
                $file_basename1 = substr($filename1, 0, strripos($filename1, '.')); // get file name
                $file_ext1 = substr($filename1, strripos($filename1, '.')); // get file extention
                $newfilename1 = $art_img1 . $file_ext1;
                
                move_uploaded_file($_FILES['student_image']['tmp_name'], "images/student/".$st_cat.'/'.$st_dir.'/'.$newfilename1);
                copy('images/student/'.$st_cat.'/'.$st_dir.'/'.$newfilename1, 'images/student/art/'.$st_cat.'/'.$newfilename1);
                $student_image = $newfilename1;
            }
            if($_FILES['student_image_sec']['name']){
                $filename2 = $_FILES["student_image_sec"]["name"];
                $file_basename2 = substr($filename2, 0, strripos($filename2, '.')); // get file name
                $file_ext2 = substr($filename2, strripos($filename2, '.')); // get file extention
                $newfilename2 = $art_img2 . $file_ext2;

                move_uploaded_file($_FILES['student_image_sec']['tmp_name'], "images/student/".$st_cat.'/'.$st_dir.'/'.$newfilename2);
                copy('images/student/'.$st_cat.'/'.$st_dir.'/'.$newfilename2, 'images/student/art/'.$st_cat.'/'.$newfilename2);
            }
            if($_FILES['student_image_thr']['name']){
                $filename3 = $_FILES["student_image_sec"]["name"];
                $file_basename3 = substr($filename3, 0, strripos($filename3, '.')); // get file name
                $file_ext3 = substr($filename3, strripos($filename3, '.')); // get file extention
                $newfilename3 = $art_img3 . $file_ext3;

                move_uploaded_file($_FILES['student_image_thr']['tmp_name'], "images/student/".$st_cat.'/'.$st_dir.'/'.$newfilename3);
                copy('images/student/'.$st_cat.'/'.$st_dir.'/'.$newfilename3, 'images/student/art/'.$st_cat.'/'.$newfilename3);
            }
            $upd_query = "update student_register set student_image='".$st_cat.'/'.$st_dir.'/'.$newfilename1."',student_image_sec='".$st_cat.'/'.$st_dir.'/'.$newfilename2."',student_image_thr='".$st_cat.'/'.$st_dir.'/'.$newfilename3."' where id='".$last_id."' ";
            $upd_rec = mysqli_query($mysqli, $upd_query);
        }
    }
    header("Location: register.php?message=Saved Successfully");

}
?>