<?php include('header.php'); ?>

<section class="flower_section layout_padding">
  <div class="container  ">
    <div class="row">
      <div class="col-md-12">
        <div class="detail-box">
          <p>
          <b>Curatorial Note</b><br>
          AJIT KUMAR G<br>
          The American Artist Shepard Fairey’s claim to fame was his Mural Arts. Strangely it was also his art form which got him into trouble with the law. He was charged with defacing the public walls around the city with graffiti, a crime which if punished could lead to a sentence of up to five years. Graffiti was viewed as an intrusion into public spaces and vandalizing by the legal systems of many countries. The police have arrested several artists for the same reason. Graffiti made an appearance initially as an expression of dissent more than as an art form. Back in 1960’s and 70’s graffiti was seen at various suburban train stations, trains and other public spaces as a means of communicating their distress by those who were sidelined by the society or race. Many graffiti artists chose to hide their identity to escape from being prosecuted. They work in the darkness of the night and disappear before daylight breaks in. The identity of Banksy, a famous graffiti artist in England is unknown to this day.
          </p>
          <p>
          Its only recently that graffiti has gathered public acceptance as a public art form. In many cities there are ongoing projects to beautify public spaces with various art installations. The beautiful paintings and art in Cities of Estonia is being promoted to increase revenue by tourism. This method is also used to promote social or political agenda. Public art is activity included in the restorative justice program at juvenile prisons in the city of Philadelphia. In India, Massive murals are exhibited in cities like New Delhi, Chennai, Goa, Bangalore, Kolkata and Thiruvananthapuram. An excellent example of public acceptance for this once scorned and illegal artform is the massive mural of Gandhi on the wall of the Delhi Police Head quarters. This collaborative work of artist Anpu Varkey and the German artist Hendrik Beikrich was completed in 2014.
          </p>
          <p>
          Actually, the murals on walls are not a modern concept. It is as old as human civilization itself. The ancient drawings on the walls of caves are truly fascinating and the recent painting discovered in Indonesia is estimated to be at least 40,000 years old. The cave drawings found at Lascaux and Chauvet in France and at Altamira in Spain is estimated to be about 20,000 years old. The Ajanta paintings of 2500 Ya and the paintings at the Brahadeeswara temple of 1000 Ya are very popular from the Indian subcontinent. In latter days there were paintings inscribed on the walls of the Krishnapuram Palace at Kayamkulam, Mattancheri Palace, Padmanabhapuram Palace and many other temples and palaces in Kerala. The traditional paintings are created using natural pigments like ochre, charcoal from oil lamps and the extract of leaves and flowers. The beautiful paintings on the Sistine Chapel by Michelangelo was done by the Fresco kind of water colours. Now the mural artists prefer the more convenient acrylic paints. A few nomadic artists can be still seen on Kerala creating art on walls with pigments from natural materials and extract from leaves.
          </p>
          <div class="img-box" style="text-align: center;">
            <img src="images/asm/Titanium-art-wall.png" alt="Titanium-art-wall.png" width="900" height="500" />
          </div><br>
          <p>
          The murals which made a comeback in the form of graffiti on public walls underwent a significant transformation. Graffiti made an appearance inside Mexican buildings in the first few decades of the twentieth century. The murals on the Berlin Wall differ on concept and creativity from the traditional art. Artists from around the world joined together to create art on the Berlin Wall after the collapse of the wall and integration between East and West Germany. Arteria, a project initiated by the Tourism Department, played an important part in bringing contemporary art to public spaces in Kerala.
          </p>
          <p>
          This art wall project on the walls of the Travancore Titanium by Amuseum Art Science finds it’s place in this multifaceted history of mural arts. Travancore Titanium is the producer of many industrial products including the Titanium Dioxide which is a colouring agent for many products used in our daily life. Art makes an incursion into the factory atmosphere of sirens, labour and dust by transforming the dull industrial factory walls at Veli into a vibrant art gallery. Waste ridden area was given a fresh breath of life when a new footpath and street lights were put in place along the art wall. Mural art along the walls about 2500 ft long, awaits your gaze all day and night. The art on these walls are very different from the graffiti on other parts of the world since the traditional graffiti or mural style is discarded in this project. Artists painted like they do on canvas in their studio, but on the huge wall instead of a canvas.
          </p>
          <p>
          The art wall has been created in two sections. The travails of the human race through the pandemic have been picturized in one section by students from the Fine Arts College of Thiruvananthapuram – Ajay K.P, Ratheesh Kumar, Thushara Balakrishnan, Ramith S, Vivek Sarej, Stephin T.S, Akhil Vinod and Vivek V.C. Twelve eminent contemporary artists who have expressed their creativity on the other section are N.N. Rimson, Prasanna Kumar, Chandranandan, Sajitha R Shankar, T.K. Hareendran, Sreelal, Vani N.M, Karakkamandapam Vijaya Kumar, O. Sundar, Pradeep Puthoor and Ajit Kumar G.
          </p>
          <p>
          The Sars Cov 2 virus is supposedly infected humans from the markets of Wuhan City in China. The first section of the art wall starts with a picture of the Wuhan market and ends with an image of the vaccine trial in human which we all hope and trust will save humanity from the virus. The paintings are a recreation of those images which were popularized through Facebook, Twitter, the news and other social media.
          </p>
          <p>
          When we heard the news of a communicable disease which cause death of thousands in Wuhan, we never realized it will essentially put the entire world into a lockdown and we will all exist like birds trapped in a cage for a year. Previously unheard-of terms like Social distancing, sanitizer, quarantine, lockdown, biometric monitoring all became part of our daily life. People adapted themselves to the scientific directives for controlling covid. Public places like religious worship, movie theatres, museums, Schools and colleges and remained closed.Classes and social gatherings were tried to maintain via zoom, google meet and other virtual mediums.
          </p>
          <p>
          Covid struck across all social strata irrespective of race, class, and other human divides and eliminated several lakhs of human lives from a destitute to a national leader. Hundreds of thousands of people including doctors and scientists succumbed to the virus. Tedros Adhanom, the Director General of the World Health Organisation made the following statement, “Covid-19 does not discriminate between rich nations and poor, large nations and small. It does not discriminate between nationalities, ethnicities or ideologies… This is a time for all of us to be united in out common struggle against a common threat, a dangerous enemy.” The covid art wall has immortalized how we collectively survived a pandemic with scientific methods in Modern Medicine and Social Engineering and co-operation.
          </p>
          <p>
          The second section of the art wall is a unique experience from the diverse artistic techniques and creativity. Images, rendering style, texture and illustration makes each painting exceptional. The painters express their mental processes as they endured the various phases of the pandemic.
          </p>
          <p>
          Chandranandan’s picture subtly hints that technological innovations like air travel is a reason for the accelerated spread of Covid and consequently the death of many. He has expressed human beings and bird with feathers spread in a novel way. Sreelal’s style of painting is minimalistic in expressing colours and leaving ample space of the white wall bare. Beautifully created brush strokes define the space into many images. The painting provokes a thought that we went through a combined phase of hunger, life, celebrations and death.
          </p>
          <p>
          The hallmark of Pradeep Puthoor’s painting is the bold black stripes in contrast to the intense yellow backdrop. The basic inspiration for the painting is the imagery of all living beings including humans melded together, in a state of decomposition or a fight for survival. Prasanna Kumar has created the images of star constellations transforming from one body to another as though hope transpires from one to the other. Karakkamandapom VijayaKumar has pictured a survivor against the challenges of disasters, despair and disease.
          </p>
          <p>
          Rimzon has created his drawings within seven feet wide circles suspended in a void space. As you view this painting on the wall you will see walls within the painting. Like the walls of a building from a by gone era. The dark spaces on the walls are the doors which seemingly open to the desperate spaces within the building. Vani N.M. has converted the art wall into a dark wall of a building. The image of a person eagerly looking out of the window express es the distress and loneliness many of us went through as we endured the lockdown and quarantine. Aji V.N. made his work as an exact replication of a charcoal drawing on the 75 feet wall, but with acrylic. He has tried to portray the isolation of people by social distancing, and the yearning for social gatherings in the future. Sajitha Sankhar has expressed the suffering of women through the pandemic life, as it happens during all other periods of their life.
          </p>

          <div class="row">
          <div class="col-md-6">
          <p>
          T.K. Hareendran’s art wall series successfully transitioned his art canvas from the gallery to the streets. He narrates the diversity of humans depicting their micro and macro spaces. A part of the art created by O. Sundar is reminiscent of the very famous painting Woman with Dead Child by Kathe Kollwitz. The image of a person lying prone on the floor looks back at us with pitiful eyes. Revisit by Ajit Kumar G picturizes a group of hominid shapes waiting eagerly as though expecting some visitors. Some of those in the painting may reminds us of historical or contemporary images. Their face and sharp gaze stands out in contrast of their hazy figures. A thin line separates between the visitors.
          </p>
          <p>
          Travancore Artwall is an attempt for effortless interaction of the people with art. Art comes closer to people. You no longer need to go in search of galleries to enjoy and appreciate art. A challenge for the artist is to show something which keeps the enthusiasm of the public. The Travancore art wall has made it possible for a constant interaction and experimentation between the artists and art lovers which opens the doors to endless possibilities of experimentation, dialogue and learning in Visual Art.
          </p>
          <p>
          Location: Compound wall.<br>
          Travancore TitaniumProducts Ltd<br>
          Kochuveli, Thiruvananthapuram, Kerala.
          </p>
          <p>
          Area: 15000 sq feet<br>
          Medium: Acrylic emulsion<br>
          Completed on 13 February 2021
          </p>
          <p>Curator: AjitKumar G</p>
          <a target="_blank" href="images/asm/artwall_catalogue.pdf"><button style="margin: 0 0 0 10px;" class="btn btn-primary"> Download Catalogue </button></a><br><br>
          </div>
          <div class="col-md-6">
            <div class="img-box" style="text-align: center;"><br><br><br>
              <img src="images/asm/ArtWall_Brochure.jpg" alt="ArtWall_Brochure.jpg" width="470" height="600" />
            </div>
          </div>
          </div>

          <div class="row">
          <div class="col-md-6">
            <img src="images/asm/Artwall_001.jpg" alt="Artwall_001.jpg" width="500" height="500" />
          </div>
          <div class="col-md-6">
            <img src="images/asm/Artwall_002.jpg" alt="Artwall_002.jpg" width="500" height="500" />
          </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php'); ?>