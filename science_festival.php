<?php include('header.php'); ?>
<!-- tellmed section -->

<section class="tellmed_section layout_padding tellmed_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="detail-box">
          <p>
          Amuseum is organising a Science Festival at Thiruvananthapuram  collaborating with international Science Festival Agencies, Educational Institutions of our State and Country and like minded Non Governmental Organisations. The Festival will carry multifaceted events featuring scores of programs and activities related to science, technology, engineering, Art and mathematics (STEAM).
          </p>
          <p>
          The science festivals are principally celebrations of Science,Technology and Creativity. The Festivals are comprised of thematically designed Science Exhibition, Public Lectures and workshops enabling hand on experience for Young students. With events taking place at a Festival Complex, laboratories, institutions, street corners, museums, libraries, and beyond. Science festivals are playing an important part in secondary of the society.
          </p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img-box">
          <img src="images/asm/science_fest.png" alt="science_fest.png">
        </div>
      </div>
    </div>
  <div class="row">
    <div class="col-md-12">
        <div class="detail-box">
          <p>Amuseum Science festival is aiming to function radically different from conventional forms of science education and communication. These festivals shift away to a more democratic dialogue by infusing curiosity and engaging the visitor deeply through unfamiliar and interesting ART meet SCIENCE methods.
          </p>
          <p>
          The First edition of the Science Festival will showcase the scientific achievements of different science disciplines. The exhibitions and programmes of these disciplines will be curated and displayed  independently at different pavilions. The major disciplines of the festival is as follows. Art and Science, Biology and Biosciences, Chemistry, Computer and Digital Technology, Environmental Science, Geology, Health Sciences, History and Philosophy of Science, Innovation and Entrepreneurship, Mathematics, Physics, Robotics, Space and Astronomy, Sustainable Development, Science and Education/ Communication, Evolution, Survival,, Climate change and Future.
          </p>
          <p>
          <b>Venue</b><br>
          Amuseum Science Festival will be conducted at various venues in the City of Thiruvananthapuram in November 2022.
          </p>
        </div>
    </div>
  </div>


  </div>
</section>
<?php include('footer.php'); ?>