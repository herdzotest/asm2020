   <!-- info section -->
<section class="info_section">
  <div class="container">
    <div class="info_contact_container">
      <h3>
        Contact Us
      </h3>
      <div class="info_contact_box">
        <p>
          AMUSEUM Artscience TC 979/26 Plamoodu -PMG Highway Pattom Post.
          Thiruvananthapuram. 695004. Kerala. India. www.amuseum.org.in
        </p>
        <a href="">
          <i class="fa fa-phone" aria-hidden="true"></i>
          <span>
            +91 9995036666, +91 8589061461
          </span>
        </a>
        <a href="">
          <i class="fa fa-envelope"></i>
          <span>
            amuseumartscience@gmail.com
          </span>
        </a>
      </div>
    </div>
    <div class="social_box">
      <a href="https://www.facebook.com/AmuseumArtscience" target="_blank">
        <i class="fa fa-facebook" aria-hidden="true"></i>
      </a>
      <a href="https://twitter.com/Amuseum6?s=09" target="_blank">
        <i class="fa fa-twitter" aria-hidden="true"></i>
      </a>
      <a href="https://instagram.com/amuseumartscience" target="_blank">
        <i class="fa fa-instagram" aria-hidden="true"></i>
      </a>
      <a href="https://www.youtube.com/channel/UCFbx-Iw789J2jfVc1ve1Xxw" target="_blank">
        <i class="fa fa-youtube-play" aria-hidden="true"></i>
      </a>
    </div>
    <div class="info_link_box">
      <a href="index.php">
        Home
      </a>
      <a href="about.php">
        About
      </a>
      <a href="programmes.php">
        Programmes
      </a>
      <a href="contact-us.php">
        Contact Us
      </a>
    </div>
  </div>
</section>

<!-- end info_section -->
   <!-- footer section -->
   <section class="container-fluid footer_section">
      <div class="container">
        <div class="col-md-11 col-lg-8 mx-auto">
          <p>
            &copy; <span id="displayYear"></span> All Rights Reserved By
            Amuseum, Powered By
            <a href="https://herdzo.com">Herdzo Innovations (P) Ltd</a>
          </p>
        </div>
      </div>
    </section>
    <!-- footer section -->

    <!-- jQery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- slick slider -->
    <script
      type="text/javascript"
      src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"
    ></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>

<!-- Gallery -->
<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<!-- End Gallery -->



  </body>
</html>