<?php include('header.php'); ?>

<section class="flower_section  about_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-12">
        <div class="detail-box">
          <div class="row">
          <div class="col-md-6">
          <div class="img-box" style="text-align: center;"><br><br>
            <img src="images/asm/programme_01.png" alt="programme_01.png" width="500" height="280" />
          </div>
          </div>
          <div class="col-md-6"><br><br>
          <p>
          <h5>1.	Sharing thoughts</h5><br>
          Sharing thoughts is a series of Lectures, Panel Discussions and Seminars on Art and Science by eminent scholars from different domains of knowledge and activities. During the Pandemic Period the Programme is conducted through ZOOM Platform. The lectures are conducted in collaboration with various Government and Non Government Institutions. The recorded videos are available in our YouTube Channel. 
          <a href="www.youtube.com/AmuseumArtscience">www.youtube.com/AmuseumArtscience</a>
          </p>
          </div>
          </div>
          
          <br><br>
          <p style="text-align: center; font-size:20px;">The upcoming lectures</p>
          <p style="text-align: center;">‘നമ്മളെങ്ങനെനമ്മളായി?’</p>
          <div class="img-box" style="text-align: center;">
            <img src="images/asm/web_card_story_of_the_universe.jpg" alt="web_card_story_of_the_universe.jpeg" width="600" height="300" />
          </div>
          
          <div class="row">
          <div class="col-md-6">
          <p>
          <h5>2. Amuseum MUTUAL ART FUND</h5><br>
          Amuseum Mutual Art Fund aims provide a support to young Artists and Amuseum. Through this project Portraits with charcoal/ Pencil/ Ink are made in Archival paper of A4 size, framed and send to the clients. Each portrait is charged INR: 1500/- including postage in India. The photographs & the Address can be sent to Amuseum through E Mail, and payment through Amuseum bank Account. The Portrait will be send to the address within a week.
          </p>
          <p>
          MAIL THE PHOTOGRAPH  TO BE DRAWN along with the POSTAL ADDRESS<br>
          <b>@ amuseumartscience@gmail.com</b>
          </p>
          </div>
          <div class="col-md-6"><br><br><br><br>
            <img src="images/asm/Mutual_Art_fund.jpg" alt="Mutual_Art_fund" width="450" height="600" />
          </div>
          </div>
  </div></div></div>

  </div>
</section>


<!-- gallery section -->
<section class="why_section layout_padding">
<div class="container">
<div class="heading_container">
  <h2>
    PREVIOUS PROGRAMMES
  </h2><br><br>
</div>
<div class="row1">
  <div class="column1">
    <img src="images/asm/pgm1.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm2.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm3.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm4.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm5.jpeg" style="width:100%" onclick="openModal();currentSlide(5)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/pgm6.jpeg" style="width:100%" onclick="openModal();currentSlide(6)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm7.jpg" style="width:100%" onclick="openModal();currentSlide(7)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm8.jpg" style="width:100%" onclick="openModal();currentSlide(8)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm9.jpg" style="width:100%" onclick="openModal();currentSlide(9)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm10.jpg" style="width:100%" onclick="openModal();currentSlide(10)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/pgm11.jpg" style="width:100%" onclick="openModal();currentSlide(11)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm12.jpg" style="width:100%" onclick="openModal();currentSlide(12)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm13.jpg" style="width:100%" onclick="openModal();currentSlide(13)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm14.jpeg" style="width:100%" onclick="openModal();currentSlide(14)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm15.jpg" style="width:100%" onclick="openModal();currentSlide(15)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/pgm16.jpg" style="width:100%" onclick="openModal();currentSlide(16)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm17.jpg" style="width:100%" onclick="openModal();currentSlide(17)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm18.jpg" style="width:100%" onclick="openModal();currentSlide(18)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm19.jpg" style="width:100%" onclick="openModal();currentSlide(19)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm20.jpg" style="width:100%" onclick="openModal();currentSlide(20)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/pgm21.jpg" style="width:100%" onclick="openModal();currentSlide(21)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm22.jpg" style="width:100%" onclick="openModal();currentSlide(22)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm23.jpg" style="width:100%" onclick="openModal();currentSlide(23)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm24.jpg" style="width:100%" onclick="openModal();currentSlide(24)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm25.jpg" style="width:100%" onclick="openModal();currentSlide(25)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/pgm26.jpg" style="width:100%" onclick="openModal();currentSlide(26)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm27.jpg" style="width:100%" onclick="openModal();currentSlide(27)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm28.jpg" style="width:100%" onclick="openModal();currentSlide(28)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm29.jpg" style="width:100%" onclick="openModal();currentSlide(29)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/pgm30.jpg" style="width:100%" onclick="openModal();currentSlide(30)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/pgm31.jpg" style="width:100%" onclick="openModal();currentSlide(31)" class="hover-shadow cursor">
  </div>
</div>

</div>
</section>
<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">
    <div class="mySlides">
      <img src="images/asm/pgm1.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm2.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm3.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm4.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm5.jpeg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/pgm6.jpeg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm7.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm8.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm9.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm10.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/pgm11.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm12.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm13.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm14.jpeg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm15.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/pgm16.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm17.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm18.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm19.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm20.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/pgm21.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm22.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm23.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm24.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm25.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/pgm26.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm27.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm28.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm29.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/pgm30.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/pgm31.jpg" style="width:100%">
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>
  </div>
</div>
<br><br>
<!-- end gallery section -->

<?php include('footer.php'); ?>