<?php include('header.php'); ?>
<!-- tellmed section -->

<section class="tellmed_section layout_padding tellmed_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="detail-box">
          <p>
          ‘A call away’<br>
          TellMED is volunteering Telemedicine Helpline towards Kerala. The programme is managed by Amuseum Artscience in association with Kerala Social Security Mission(Govt of Kerala) and various NGOs around the Globe including UUKMA, UNF, Unarv, Caring Hands, World Malayali Council etc. The programme is a free Telemedicine service to the People of India. Its managed through volunteering service from Keralite Health care professionals from different countries and Kerala.
          </p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img-box">
        <br><br><img src="images/asm/tellmed_logo_03.png" alt="tellmed_logo_03.png" width="400" height="200" />
        </div>
      </div>
    </div>
  <div class="row">
    <div class="col-md-12">
        <div class="detail-box">
          <p>
          <b>Aims & Objectives:</b><br>
          1. Telemedicine and telemedicine advice to patients of asymptomatic, mild & moderate cases in Domiciliary Care.<br>
          2. Supporting Indian Medical & Nursing team across India through telemedicine, sharing approved PPGs in accordance with best international evidence-based practice.<br>
          3. Reaching out to the general public with essential medical & non-medical supplies.<br>
          4. Mass education to the general public regarding Covid-19.<br>
          5. Reduce the influx of patients with mild symptoms to acute care hospitals and maximise the availability of beds for acutely ill patients.<br>
          </p>
          <p><b>How it works:</b><br>
          1. Tell MED provides simple telephonic support for those people who experience difficulties with Video Telemedicine Platform.<br>
          2. When both the client and Health Professional are comfortable with Telemedine Video conversation will direct their calls to TeleMedicine Platform,Unarv.<br>
          3. Calls will be received by HR manager/ Volunteer and Directed to Consultants after verifying it as a NON Emergency Enquiry.<br>
          </p>
          <p>
          Connect to tellMED<br>
          Call: <b>85890 61461<br>
          9995036666</b><br>
          </p>
          <p>
          Alternatively Join<br>
          <b>www.covid.unarv.com</b><br>
          Password:10<br>
          Or<br>
          Zoom Webinar ID: <b>863 4365 0001</b><br>
          Please click the link below to join the webinar:<b> <a href="https://us02web.zoom.us/j/86343650001">https://us02web.zoom.us/j/86343650001</a> </b><br>
          </p>
          
    <div class="row">
      <div class="col-md-6">
          <div class="img-box">
            <img src="images/asm/tell_med-01.jpg" alt="tell_med-01.jpg" width="500" height="600" />
          </div>
      </div>
      <div class="col-md-6">
          <div class="img-box">
            <img src="images/asm/tell-med_02.png" alt="tell-med_02.png" width="600" height="350" />
          </div>
      </div>
    </div>

        </div>
    </div>
  </div>


  </div>
</section>
<?php include('footer.php'); ?>