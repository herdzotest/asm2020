<?php include('header.php'); ?>
<!-- tellmed section -->

<section class="tellmed_section layout_padding tellmed_flower_common_section">
  <div class="container  ">
    <h3>WINNERS - JUNIOR</h3>
    <div class="row">
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_winner_01.jpeg" alt="junior_winner_01.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_winner_02.jpeg" alt="junior_winner_02.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_winner_03.jpeg" alt="junior_winner_03.jpeg" width="360" height="250" />
        </div>
      </div>
    </div><br><br>
    <h3>WINNERS - SENIOR</h3>
    <div class="row">
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_winner_01.jpeg" alt="senior_winner_01.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_winner_02.jpeg" alt="senior_winner_02.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_winner_03.jpeg" alt="senior_winner_03.jpeg" width="360" height="250" />
        </div>
      </div>
    </div><br><br>
    <h3>PAINTINGS - JUNIOR</h3>
    <div class="row">
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_painting_01.jpeg" alt="junior_painting_01.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_painting_02.jpeg" alt="junior_painting_02.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/junior_painting_03.jpeg" alt="junior_painting_03.jpeg" width="360" height="250" />
        </div>
      </div>
    </div><br><br>
    <h3>PAINTINGS - SENIOR</h3>
    <div class="row">
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_painting_01.jpeg" alt="senior_painting_01.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_painting_02.jpeg" alt="senior_painting_02.jpeg" width="360" height="250" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="img-box">
          <img src="images/asm/senior_painting_03.jpg" alt="senior_painting_03.jpg" width="360" height="250" />
        </div>
      </div>
    </div><br><br>
    <h3>JURY MEMBERS</h3>
    <div class="row jury-div">
      <div class="col-md-3">
        <div class="img-box">
          <img src="images/asm/sushama_asm.webp" alt="Sushma Sabnis" style="width: 250px; height:300px" />
        </div>
      </div>
      <div class="col-md-9">
        <p style="text-align: justify;"><strong>Sushma Sabnis</strong> is a marine biologist by training, Sushma Sabnis has been pursuing her interest in painting and writing since she left her career as a scientist over a decade ago. With a few solo and group exhibitions to her credit in Mumbai and Delhi, Sushma has also been instrumental in bringing a few young contemporaries together for a series of curated exhibitions called Advaita, a platform for arts created by her. Her essays and features on Art have appeared in noted art journals including Art & Deal and Creative Minds, Arts Illustrated. She has been a contributing editor to the online magazine CartAnArt. She also manages The Art Daily and Art Tehelka, two online platforms for art writing, and her reviews, features and interviews about art shows, art festivals have appeared in leading newspapers. Sushma has also worked as a curatorial assistant for certain high profile shows and art projects, biennale and has curated a few solo and group shows in the recent times of artists from Mumbai and Karnataka. She also works as a communications coordinator and art consultant for various art initiatives, ventures and art organizations in Mumbai.</p>
      </div>
    </div>
    <div class="row jury-div">
      <div class="col-md-3">
        <div class="img-box">
          <img src="images/asm/o_sunder_asm.jpg" alt="P Sundar" style="width: 250px; height:300px" />
        </div>
      </div>
      <div class="col-md-9">
        <p><strong> O Sunder</strong> graduated in Painting from RLV College of Fine Arts, Thripunithura. He Participated in more than 60 shows all over India, including solos. He founded and curates the Cochin Art Fair. Currently he is the Director-Cochin Artcube. lives and works in Kochi.</p>
      </div>
    </div>
    <div class="row jury-div">
      <div class="col-md-3">
        <div class="img-box">
          <img src="images/asm/Lina-asm.jpg" alt="Lina Vincent" style="width: 250px; height:300px" />
        </div>
      </div>
      <div class="col-md-9">
        <p><strong> Lina Vincent</strong> is an independent art historian and curator with two decades of experience in arts management. She is committed to socially engaged practices that reflect in the multi-disciplinary projects she has developed and participated in. The focus areas of her research include arts education, printmaking history and practice, the documentation of living traditions, and environmental consciousness in the arts. She recently worked on an Archival Museum Fellowship from the India Foundation for the Arts, and runs the Goa Familia archival photography project with the Serendipity Arts Foundation. She initiated and headed the Piramal Residency Artist Incubator Programme 2019-20. Lina has curated numerous exhibitions with galleries across India and contributes to publications on art history and contemporary cultural practices.</p>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php'); ?>