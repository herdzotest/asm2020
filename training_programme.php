<?php include('header.php'); ?>

<section class="flower_section  about_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-12">
        <div class="detail-box">
          <div class="row">
            <div class="col-md-6"><br><br><br>
              <p>Covid 19<br> 
              <b>Safety Training Programme</b><br>
              For Legislative Assembly Members<br> 
              </p>
              <p>Conducted on <b>9/6/21</b> at R Sankara Narayanan Thampi Members Lounge<br>
              Assembly Building, Thiruvananthapuram<br>
              </p>
              <p>
              In association with<br>
              Kerala Legislative Assembly Secretariat<br>
              &<br>
              Govt Medical College, Trivandrum 
              </p>
            </div>
          </div>
  </div></div></div>
  </div>
</section>

<!-- gallery section -->
<section class="why_section layout_padding">
<div class="container">
<div class="row1">
  <div class="column1">
    <img src="images/asm/tr_pgm1.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm2.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm3.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm4.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm5.jpg" style="width:100%" onclick="openModal();currentSlide(5)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/tr_pgm6.jpg" style="width:100%" onclick="openModal();currentSlide(6)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm7.jpg" style="width:100%" onclick="openModal();currentSlide(7)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm8.jpg" style="width:100%" onclick="openModal();currentSlide(8)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm9.jpg" style="width:100%" onclick="openModal();currentSlide(9)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm10.jpg" style="width:100%" onclick="openModal();currentSlide(10)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/tr_pgm11.jpg" style="width:100%" onclick="openModal();currentSlide(11)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm12.jpg" style="width:100%" onclick="openModal();currentSlide(12)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm13.jpg" style="width:100%" onclick="openModal();currentSlide(13)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm14.jpg" style="width:100%" onclick="openModal();currentSlide(14)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm15.jpg" style="width:100%" onclick="openModal();currentSlide(15)" class="hover-shadow cursor">
  </div>
</div><br><br>

<div class="row1">
  <div class="column1">
    <img src="images/asm/tr_pgm16.jpg" style="width:100%" onclick="openModal();currentSlide(16)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm17.jpg" style="width:100%" onclick="openModal();currentSlide(17)" class="hover-shadow cursor">
  </div>
  <div class="column1">
    <img src="images/asm/tr_pgm18.jpg" style="width:100%" onclick="openModal();currentSlide(18)" class="hover-shadow cursor">
  </div>
</div><br><br>
</div>
</section>
<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">
    <div class="mySlides">
      <img src="images/asm/tr_pgm1.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm2.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm3.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm4.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm5.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/tr_pgm6.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm7.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm8.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm9.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm10.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/tr_pgm11.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm12.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm13.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm14.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm15.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <img src="images/asm/tr_pgm16.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm17.jpg" style="width:100%">
    </div>
    <div class="mySlides">
      <img src="images/asm/tr_pgm18.jpg" style="width:100%">
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>
  </div>
</div>
<br><br>
<!-- end gallery section -->

<?php include('footer.php'); ?>