<?php include('header.php'); ?>
<!-- about section -->

<section class="about_section layout_padding about_flower_common_section">
  <div class="container  ">
    <div class="row">
      <div class="col-md-6">
        <div class="detail-box">
          <p>
          ‘Amuseum is a new age concept foundation for Artscience Philosophy established in Trivandrum, the capital city of Kerala, India. Amuseum is a collective of independent intellectuals, artists, scientists and cultural enthusiasts from different parts of the Globe. It aims to emphasis the value of interdisciplinary inquiry bringing together Art and Science in experiencing and understanding the world around us in a broader and deeper way.
          </p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img-box">
          <img src="images/asm/amuseum in roundA low.png" alt="amuseum in roundA low.png" width="300" height="300">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="detail-box">
        <p><b>The aims and objectives:</b><br>
          1. To carry out activities that provide space, opportunities and support to Art and Science Projects, public art projects, and to function as an anchor of Art and the public.<br>

          2. To provide a centre to make art exhibitions, seminars, workshops, studio facilities and research on Art, Culture, Science and Technology.<br>

          3. To engage the society with Science through Discussions, Seminars and Science exhibitions to evolve into a Science friendly Society.<br>

          4. To educate the methodology and objectivity of science in the society.<br>

          5. To act as a point of intersection between science and art to provide rich ground for collaborative working, questioning paradigms and engaging new audiences.<br>

          6. To celebrate Creativity, merging ART with SCIENCE<br>
          </p>
          <p>"Scientists and scholars in the humanities, working together, will, I believe, serve as the leaders of a new philosophy, one that blends the best and most relevant from these two great branches of learning. Their effort will be the third Enlightenment. Unlike the first two, this one might well endure.” 
― Edward O. Wilson, The Origins of Creativity
        </p>
        </div>
      </div>
    </div>


  </div>
</section>
<?php include('footer.php'); ?>